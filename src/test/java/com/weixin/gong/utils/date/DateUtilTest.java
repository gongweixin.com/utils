package com.weixin.gong.utils.date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * @author weixin.gong
 * @date 15-7-21 上午11:36
 */
public class DateUtilTest {

    @Test
    public void testGap() throws Exception {
        FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");
        Date date1;
        Date date2;

        date1 = fastDateFormat.parse("2015-01-01 00:00:01");
        date2 = fastDateFormat.parse("2014-12-31 23:59:00");
        Assert.assertEquals(DateUtil.gap(date1, date2), 1);

        date1 = new Date();
        date2 = new Date();
        Assert.assertEquals(DateUtil.gap(date1, date2), 0);
    }

    @Test
    public void testFormat() throws Exception {
        Assert.assertEquals(DateUtil.format(108605000), "30:10:05");
        Assert.assertEquals(DateUtil.format(605000), "00:10:05");
        Assert.assertEquals(DateUtil.format(545000), "00:09:05");
        Assert.assertEquals(DateUtil.format(5000), "00:00:05");
    }
}