package com.weixin.gong.utils.collection;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author weixin.gong
 * @date 15-11-4 下午2:40
 */
public class ListUtil {
    public static <T> List toList(T object) {
        if (object == null) {
            return Lists.newArrayList();
        }
        if (object instanceof  List) {
            return (List)object;
        }
        List<T> list = Lists.newArrayList();
        list.add(object);
        return list;
    }

    public static <T> List toListNoEmptyStr(T object) {
        if (Objects.equal(object, "")) {
            return Lists.newArrayList();
        }
        return toList(object);
    }
}
