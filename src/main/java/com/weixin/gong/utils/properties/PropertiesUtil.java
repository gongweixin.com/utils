package com.weixin.gong.utils.properties;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: gongweixin
 * Date: 14-7-28
 * Time: 下午11:48
 * To change this template use File | Settings | File Templates.
 */
public class PropertiesUtil {
	private static final Properties properties = new Properties();

	static {
		try {
			properties.load(PropertiesUtil.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (Exception e) {
			throw new RuntimeException("no find config.properties");
		}
	}

	public static String get(String key) {
		return properties.getProperty(key);
	}
}
