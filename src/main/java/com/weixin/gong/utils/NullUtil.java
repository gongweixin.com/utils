package com.weixin.gong.utils;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * @author weixin.gong
 * @date 15-11-3 上午11:27
 */
public class NullUtil {
    public static <T> List<T> nullToEmpty(List<T> list) {
        return MoreObjects.firstNonNull(list, Lists.<T>newArrayList());
    }

    public static <K, V> Map<K, V> nullToEmpty(Map<K, V> map) {
        return MoreObjects.firstNonNull(map, Maps.<K, V>newHashMap());
    }
}
