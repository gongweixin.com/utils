package com.weixin.gong.utils.image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: gongweixin
 * Date: 14-7-14
 * Time: 下午4:23
 * To change this template use File | Settings | File Templates.
 */
public class ImageUtil {
	public static String getSize(File file) {
		try {
			BufferedImage bi = ImageIO.read(file);
			return bi.getWidth() + "*" + bi.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "0*0";
	}

	public static String getSize(InputStream inputStream) {
		try {
			BufferedImage bi = ImageIO.read(inputStream);
			return bi.getWidth() + "*" + bi.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
			return "0*0";
		} finally {
			try{
				inputStream.close();
				inputStream = null;
			} catch (IOException e){
				inputStream = null;
			} finally {
				inputStream = null;
			}
		}
	}
}
