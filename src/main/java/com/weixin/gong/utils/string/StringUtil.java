package com.weixin.gong.utils.string;

import org.springframework.web.util.HtmlUtils;

/**
 * @author weixin.gong
 * @date 15-6-14 下午1:11
 */
public class StringUtil {

    /**
     * html编码，如'<'编码为'&lt;'
     */
    public static String htmlEncoder(String s) {
        return HtmlUtils.htmlEscape(s);
    }
}
