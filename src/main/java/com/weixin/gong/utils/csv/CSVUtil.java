package com.weixin.gong.utils.csv;

import com.google.common.collect.Lists;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;

/**
 * 依赖
    <dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-csv</artifactId>
    <version>1.2</version>
    </dependency>
 *
 * @author weixin.gong
 * @date 15-10-8 下午9:09
 */
public class CSVUtil {

    public static List<Map<String, String>> parse(String filePath, String... header) throws IOException {
        List<Map<String, String>> mapList = Lists.newArrayList();
        Reader in = new FileReader(filePath);
        Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader(header).parse(in);
        for (CSVRecord record : records) {
            mapList.add(record.toMap());
        }
        return mapList;
    }
}
