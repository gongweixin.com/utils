package com.weixin.gong.utils.random;

import java.util.Random;

/**
 * @author weixin.gong
 * @date 15-10-12 下午8:02
 */
public class RandomUtil {
    private static final Random random = new Random();

    /**
     * 生成n位随机数字
     */
    public static String getStringNumber(int n) {
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }
}
