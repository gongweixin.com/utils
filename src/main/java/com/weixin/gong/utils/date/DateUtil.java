package com.weixin.gong.utils.date;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * @author weixin.gong
 * @date 15-6-14 下午1:12
 */
public class DateUtil {

    /**
     * 两个日期相差几天
     */
    public static int gap(Date start, Date end) throws ParseException {
        start = DateFormatUtils.ISO_DATE_FORMAT.parse(DateFormatUtils.ISO_DATE_FORMAT.format(start));
        end = DateFormatUtils.ISO_DATE_FORMAT.parse(DateFormatUtils.ISO_DATE_FORMAT.format(end));
        //一天86400000毫秒
        return Math.abs((int)(end.getTime()/86400000) - (int)(start.getTime()/86400000));
    }

    /**
     * 毫秒数转成xx小时xx分钟xx秒
     */
    public static String format(long time) {
        int hour = (int)(time / 3600000);
        time = time - hour * 3600000;
        int minute = (int)(time / 60000);
        time = time - minute * 60000;
        int second = (int)(time / 1000);
        String format = "";
        if (hour < 10) {
            format += "0";
        }
        format += hour + ":";
        if (minute < 10) {
            format += "0";
        }
        format += minute + ":";
        if (second < 10) {
            format += "0";
        }
        format += second;
        return format;
    }
}
