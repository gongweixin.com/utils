package com.weixin.gong.utils.classpath;

/**
 * @author weixin.gong
 * @date 16-7-7 上午11:40
 */
public class ClassPathUtil {

    public static String getRootPath() {
        return ClassPathUtil.class.getResource("/").getPath();
    }

    public static String getRootPath2() {
        return Thread.currentThread().getContextClassLoader().getResource("").getPath();
    }

    public static String getRootPath3() {
        return ClassPathUtil.class.getClassLoader().getResource("").getPath();
    }

    public static String getRootPath4() {
        return ClassLoader.getSystemResource("").getPath();
    }
}
