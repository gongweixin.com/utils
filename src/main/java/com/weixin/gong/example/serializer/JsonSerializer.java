package com.weixin.gong.example.serializer;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * @author weixin.gong
 * @date 15-7-6 下午5:31
 */
public class JsonSerializer<T> {

    public T fromBytes(byte[] bits, Class<T> c) {
        return JSONObject.parseObject(bits, c);
    }

    public byte[] toBytes(T var) {
        return JSONObject.toJSONString(var).getBytes();
    }




    public static void main(String[] args) {
        TestObject test = TestObject.newTestObject();
        System.out.println(test);
        byte[] bytes = new JsonSerializer<TestObject>().toBytes(test);
        System.out.println(new JsonSerializer<TestObject>().fromBytes(bytes, TestObject.class));
    }

    public enum Type {
        INSERT(1,2),UPDATE(3,4);

        private int i;
        private int j;
        private Type(int i, int j) {
            this.i = i;
            this.j = j;
        }
    }

    static class TestObject {
        private int i;
        private Long l;
        private List<TestObject> list;
        private Map<String, TestObject> map;
        private Type type;

        public TestObject() {
        }

        public TestObject(int i, Long l, Type type) {
            this.i = i;
            this.l = l;
            this.type = type;
        }

        public static TestObject newTestObject() {
            TestObject test = new TestObject(1, 1L, Type.INSERT);
            TestObject t1 = new TestObject(2, 2L, Type.INSERT);
            TestObject t2 = new TestObject(3, 3L, Type.UPDATE);
            TestObject t3 = new TestObject(4, 4L, Type.UPDATE);
            List<TestObject> list = Lists.newArrayList();
            list.add(t1);
            list.add(t2);
            test.list = list;
            Map<String, TestObject> map = Maps.newHashMap();
            map.put("t3", t3);
            test.map = map;
            return test;
        }

        @Override public String toString() {
            return "TestObject{" +
                    "i=" + i +
                    ", l=" + l +
                    ", list=" + list +
                    ", map=" + map +
                    ", type=" + type +
                    '}';
        }

        public int getI() {
            return i;
        }

        public void setI(int i) {
            this.i = i;
        }

        public Long getL() {
            return l;
        }

        public void setL(Long l) {
            this.l = l;
        }

        public List<TestObject> getList() {
            return list;
        }

        public void setList(List<TestObject> list) {
            this.list = list;
        }

        public Map<String, TestObject> getMap() {
            return map;
        }

        public void setMap(Map<String, TestObject> map) {
            this.map = map;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }
    }
}
