package com.weixin.gong.example.umeng;

/**
 * Created with IntelliJ IDEA.
 * User: gongweixin
 * Date: 14-7-21
 * Time: 下午2:19
 * To change this template use File | Settings | File Templates.
 */
public class PushFactory {

	public static Push simpleUnicastMessage(String device_tokens, String custom) {
		Push simpleUnicast = Push.unicast(device_tokens);
		Payload message = Payload.message(custom);
		simpleUnicast.setPayload(message);
		return simpleUnicast;
	}

	public static void main(String[] args) {
		simpleUnicastMessage("", "测试").send();
	}
}
