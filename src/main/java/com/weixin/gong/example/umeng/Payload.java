package com.weixin.gong.example.umeng;

/**
 * Created with IntelliJ IDEA.
 * User: gongweixin
 * Date: 14-7-21
 * Time: 下午4:02
 * To change this template use File | Settings | File Templates.
 */
public class Payload {
	private static final String NOTIFICATION = "notification";
	private static final String MESSAGE = "message";
	private String display_type;
	private Body body;

	private Payload() {
	}

	public static Payload message(String custom) {
		Payload message = new Payload();
		message.display_type = MESSAGE;
		message.body = new Body();
		message.body.custom = custom;
		return message;
	}

	/*public Payload notification() {
		Payload payload = new Payload();
		payload.display_type = NOTIFICATION;
		return payload;
	}*/

	public void setBody(Body body) {
		this.body = body;
	}

	public static class Body {
		private String ticker;
		private String title;
		private String text;
		private Integer builder_id;
		private String icon;
		private String largeIcon;
		private String img;
		private String play_vibrate;
		private String play_lights;
		private String play_sound;
		private String sound;
		private String after_open;
		private String url;
		private String activity;
		private String custom;

		public String getCustom() {
			return custom;
		}
	}

}