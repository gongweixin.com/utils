//package com.weixin.gong.example.bcs;
//
//import com.baidu.inf.iis.bcs.BaiduBCS;
//import com.baidu.inf.iis.bcs.auth.BCSCredentials;
//import com.baidu.inf.iis.bcs.auth.BCSSignCondition;
//import com.baidu.inf.iis.bcs.http.ClientConfiguration;
//import com.baidu.inf.iis.bcs.http.HttpMethodName;
//import com.baidu.inf.iis.bcs.model.ObjectListing;
//import com.baidu.inf.iis.bcs.model.ObjectMetadata;
//import com.baidu.inf.iis.bcs.request.GenerateUrlRequest;
//import com.baidu.inf.iis.bcs.request.GetObjectRequest;
//import com.baidu.inf.iis.bcs.request.ListObjectRequest;
//import com.baidu.inf.iis.bcs.request.PutObjectRequest;
//import com.baidu.inf.iis.bcs.response.BaiduBCSResponse;
//
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//
//public class BcsHandler {
//    private volatile static BcsHandler bh;
//	// ----------------------------------------
//	public static final String host = "bcs.duapp.com";
//	public static final String accessKey = "0B7a42084cfa39d5b317599e910236c8";
////	public static final String accessKey = "18d83aa055001598c9744938dbd135ff";
//	public static final String secretKey = "3513f7e2d539b07c9491526575e87999";
////	public static final String secretKey = "0E275a586bb254fa597ec9db02e5581e";
//	public static final String bucket = "myshowimage";
//	// ----------------------------------------
//	private BaiduBCS baiduBCS;
//
//	private BcsHandler() {
//		BCSCredentials credentials = new BCSCredentials(accessKey, secretKey);
//        ClientConfiguration configuration = new ClientConfiguration();
//        configuration.setMaxConnections(800);
//        configuration.setMaxConnectionsPerRoute(400);
//        configuration.setMaxErrorRetry(5);
//        baiduBCS = new BaiduBCS(credentials, host, configuration);
//		// baiduBCS.setDefaultEncoding("GBK");
//		baiduBCS.setDefaultEncoding("UTF-8"); // Default UTF-8
//	}
//	public static BcsHandler getInstence(){
//		if(bh == null) {
//			synchronized (BcsHandler.class) {
//				if (bh == null) {
//					bh = new BcsHandler();
//				}
//			}
//		}
//		return bh;
//	}
//
//    public static BaiduBCS getBaiduBCS() {
//        BaiduBCS baiduBCS;
//        BCSCredentials credentials = new BCSCredentials(accessKey, secretKey);
//        ClientConfiguration configuration = new ClientConfiguration();
//        configuration.setMaxConnections(20);
//        configuration.setMaxConnectionsPerRoute(5);
//        configuration.setMaxErrorRetry(5);
//        baiduBCS = new BaiduBCS(credentials, host, configuration);
//        // baiduBCS.setDefaultEncoding("GBK");
//        baiduBCS.setDefaultEncoding("UTF-8"); // Default UTF-8
//        return baiduBCS;
//    }
//
//	public String generateUrl(String object) {
//		GenerateUrlRequest generateUrlRequest = new GenerateUrlRequest(HttpMethodName.GET, bucket, object);
//		generateUrlRequest.setBcsSignCondition(new BCSSignCondition());
////		generateUrlRequest.getBcsSignCondition().setIp("1.1.1.1");
////		generateUrlRequest.getBcsSignCondition().setTime(123455L);
////		generateUrlRequest.getBcsSignCondition().setSize(123455L);
//		return baiduBCS.generateUrl(generateUrlRequest);
//	}
//
//	public void putObjectByFile(String object, File file) {
//		PutObjectRequest request = new PutObjectRequest(bucket, object, file);
//		ObjectMetadata metadata = new ObjectMetadata();
//		// metadata.setContentType("text/html");
//		request.setMetadata(metadata);
//		BaiduBCSResponse<ObjectMetadata> response = getBaiduBCS().putObject(request);
//		ObjectMetadata objectMetadata = response.getResult();
//	}
//
//	private void listObject() {
//		ListObjectRequest listObjectRequest = new ListObjectRequest(bucket);
//		listObjectRequest.setStart(0);
//		listObjectRequest.setLimit(20);
//		// ------------------by dir
//		{
//			// prefix must start with '/' and end with '/'
//			// listObjectRequest.setPrefix("/1/");
//			// listObjectRequest.setListModel(2);
//		}
//		// ------------------only object
//		{
//			// prefix must start with '/'
//			// listObjectRequest.setPrefix("/1/");
//		}
//		BaiduBCSResponse<ObjectListing> response = baiduBCS.listObject(listObjectRequest);
////		log.info("we get [" + response.getResult().getObjectSummaries().size() + "] object record.");
////		for (ObjectSummary os : response.getResult().getObjectSummaries()) {
////			log.info(os.toString());
////		}
//	}
//
//	public void putObjectByInputStream(String object, InputStream fileContent) throws IOException {
//		ObjectMetadata objectMetadata = new ObjectMetadata();
//		objectMetadata.setContentType("text/html");
//		objectMetadata.setContentLength(fileContent.available());
//		PutObjectRequest request = new PutObjectRequest(bucket, object, fileContent, objectMetadata);
//		BaiduBCSResponse<ObjectMetadata> response = baiduBCS.putObject(request);
//		ObjectMetadata result = response.getResult();
//		System.out.println(result.toString());
//	}
//
//    public Object getObject(String object) {
//        GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, object);
//        BaiduBCSResponse response = null;
//        try {
//            response = getBaiduBCS().getObject(getObjectRequest);
//        } catch (Exception e) {
//            System.out.println("数据不存在！！准备上传 file="+object);
//        }
//
//        return response;
//    }
//}
