package com.weixin.gong.example.lombok;

import lombok.Data;

/**
 * 使用lombok中@Data注解可以自动生成get set方法，在代码编译时修改字节码实现
 * lombok的官网：https://projectlombok.org/
 * intellij idea 安装lombok插件就可以拥有方法的自动提示
 * @author weixin.gong
 * @date 15-9-15 下午8:35
 */
@Data
public class Person {
    private long id;
    private String name;

    public static void main(String[] args) {
        Person p = new Person();
        p.setName("123");
        System.out.println(p.getName());
    }
}
