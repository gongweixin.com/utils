package com.weixin.gong.example.aliyunoss.thumbnails;

/**
 * Created with IntelliJ IDEA.
 * User: gongweixin
 * Date: 14-7-3
 * Time: 下午12:14
 * To change this template use File | Settings | File Templates.
 */
public class ThumbnailsFactory {
	private static Thumbnails _100_100_jpg = new Thumbnails(100,100,"jpg");
	private static Thumbnails _450_450_jpg = new Thumbnails(450,450,"jpg");
	private static Thumbnails _450_450_90_jpg = new Thumbnails(450,450,90,"jpg");
	private static Thumbnails _450_450_90_false_jpg = new Thumbnails(450,450,90,false,"jpg");

	public static Thumbnails get_100_100_jpg(){
		return _100_100_jpg;
	}

	public static Thumbnails get_450_450_jpg(){
		return _450_450_jpg;
	}

	public static Thumbnails get_450_450_90_jpg(){
		return _450_450_90_jpg;
	}

	public static Thumbnails get_450_450_90_false_jpg(){
		return _450_450_90_false_jpg;
	}

	public static Thumbnails newInstant(int width,int height,String type){
		return new Thumbnails(width,height,type);
	}

	public static Thumbnails newInstant(int width,int height,int quality,String type){
		return new Thumbnails(width,height,quality,type);
	}
}
