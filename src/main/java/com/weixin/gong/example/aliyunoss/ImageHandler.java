package com.weixin.gong.example.aliyunoss;

import com.aliyun.openservices.ClientConfiguration;
import com.aliyun.openservices.ClientException;
import com.aliyun.openservices.oss.OSSClient;
import com.aliyun.openservices.oss.OSSException;
import com.aliyun.openservices.oss.model.ObjectMetadata;
import com.aliyun.openservices.oss.model.PutObjectResult;
import com.weixin.gong.example.aliyunoss.thumbnails.Thumbnails;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

/**
 * Created by CheShun on 2014/4/19.
 */
public class ImageHandler {
    static String accessKeyId = "vgF1ykzOFMvCAXUS";
    static String accessKeySecret = "DlrCHoO9HQDyh6EF7i3w7dFB1Q1nGX";
    static String endpoint = "http://oss.aliyuncs.com";
    static final long TIMEOUT = 3600 * 1000;
    static String bucketName = "imagedatatest";
	static String imageToUrl = "image.591ku.com";
	static String imageFromUrl = "imagedatatest.oss.aliyuncs.com";
    static ClientConfiguration conf;
    static {
        conf = new ClientConfiguration();
        // 设置HTTP最大连接数为10
        conf.setMaxConnections(10);

        // 设置TCP连接超时为5000毫秒
        conf.setConnectionTimeout(5000);

        // 设置最大的重试次数为3
        conf.setMaxErrorRetry(3);

        // 设置Socket传输数据超时的时间为2000毫秒
        conf.setSocketTimeout(2000);
    }

    // 初始化一个OSSClient
    static OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret, conf);

    public static OSSClient getClient() {
        return client;
    }

    // 上传文件
    public static void uploadFile(OSSClient client, String key, File file)
            throws OSSException, ClientException, FileNotFoundException {

        ObjectMetadata objectMeta = new ObjectMetadata();
        objectMeta.setContentLength(file.length());
        // 可以在metadata中标记文件类型
        objectMeta.setContentType("image/jpeg");

        InputStream input = new FileInputStream(file);
        PutObjectResult result = client.putObject(bucketName, key, input, objectMeta);
        System.out.println(result.getETag());

    }

    // 下载文件URL
    public static String downloadURL(OSSClient client, String key)
            throws OSSException, ClientException {
        return downloadURL(client, key, TIMEOUT);
    }

    public static String downloadURL(OSSClient client, String key, long timeout) {
        Date expire = new Date(new Date().getTime() + timeout);
        return client.generatePresignedUrl(bucketName, key, expire).toString();
    }

	public static String thumbnailsURL(OSSClient client, String key, Thumbnails thumbnails){
		return downloadURL(client,key + thumbnails.getStyle())
				.replace(imageFromUrl, imageToUrl);
	}

    public static void main(String[] args) {
        ImageHandler handler = new ImageHandler();

//        try {
//            File file = new File("C:/Users/CheShun/Pictures/Picture0002.jpg");
//            uploadFile(handler.getClient(), "a/" + file.getName(), file);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

        String url = downloadURL(handler.getClient(), "20140517/201405171617388441400314519655.jpg");
        System.out.println(url);
    }
}
