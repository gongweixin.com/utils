package com.weixin.gong.example.aliyunoss.thumbnails;

/**
 * Created with IntelliJ IDEA.
 * User: gongweixin
 * Date: 14-7-3
 * Time: 上午11:36
 * To change this template use File | Settings | File Templates.
 */
public class Thumbnails {
	private int width;
	private int height;
	private int quality = 100;
	boolean alignWidth = true;
	private String type;
	private String style;
	private String size;

	private Thumbnails(int width,int height){
		this.width = width;
		this.height = height;
		this.size = width + "*" + height;
	}

	private Thumbnails(int width,int height,boolean alignWidth){
		this(width,height);
		this.alignWidth = alignWidth;
	}

	public Thumbnails(int width,int height,String type){
		this(width,height);
		this.type = type;
		style = "@"+width+"w_"+height+"h." + type;
	}

	public Thumbnails(int width,int height,boolean alignWidth,String type){
		this(width,height,alignWidth);
		this.type = type;
		if(alignWidth){
			style = "@"+width+"w_"+height+"h." + type;
		} else {
			style = "@"+width+"w_"+height+"h_1e_1c." + type;
		}
	}

	public Thumbnails(int width,int height,int quality,String type){
		this(width,height);
		this.type = type;
		this.quality = quality;
		style = "@"+width+"w_"+height+"h_" + quality + "Q." + type;
	}

	public Thumbnails(int width,int height,int quality,boolean alignWidth,String type){
		this(width,height,alignWidth);
		this.type = type;
		this.quality = quality;
		if(alignWidth){
			style = "@"+width+"w_"+height+"h_" + quality + "Q." + type;
		} else {
			style = "@"+width+"w_"+height+"h_" + quality + "Q_1e_1c." + type;
		}
	}

	public String getStyle() {
		return style;
	}

	public String getSize() {
		return size;
	}

	public String getCutSize(int width,int height){
		if(width/this.width < height/this.height){
			return width*this.height/height + "*" + this.height;
		} else {
			return this.height + "*" + height*this.width/width;
		}
	}

}
