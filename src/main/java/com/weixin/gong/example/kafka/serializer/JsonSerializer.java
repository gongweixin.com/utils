package com.weixin.gong.example.kafka.serializer;

import com.alibaba.fastjson.JSONObject;
import kafka.serializer.Encoder;
import kafka.utils.VerifiableProperties;

/**
 * @author weixin.gong
 * @date 15-7-6 下午5:31
 */
public class JsonSerializer<T> implements Encoder<T> {

    public JsonSerializer(VerifiableProperties props) {
        //Encoder必须有这么个构造函数
    }

    public T fromBytes(byte[] bits, Class<T> c) {
        return JSONObject.parseObject(bits, c);
    }

    public byte[] toBytes(T var) {
        return JSONObject.toJSONString(var).getBytes();
    }
}
