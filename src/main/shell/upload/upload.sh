#!/bin/bash
username=root
target_dir=/mydata/software/tomcat/webapps
src_dir=./
src_file=web.war
ip1="x.x.x.1"
ip2="x.x.x.2"
ips="$ip_test $ip_brush"
for ip in $ips
do
    cd $src_dir
    tar -cf ${src_file}.tar $src_file
    scp ${src_file}.tar $username@$ip:$target_dir
    ssh $username@$ip > /dev/null 2>&1 <<eeooff
    cd $target_dir
    tar -xf ${src_file}.tar
    rm ${src_file}.tar
    cd ../bin
    ./shutdown.sh
    sleep 5
    ./startup.sh
    exit
eeooff
done
#./shutdown.sh 方法可以用ps -ef | grep tomcat | grep root | grep -v grep | awk {'print $2'} | xargs kill -9 替换
#./shutdown.sh 方法无法保证tomcat一定会被关闭，但替换方法可能会多杀或杀错tomcat，需要自己调整grep命令参数