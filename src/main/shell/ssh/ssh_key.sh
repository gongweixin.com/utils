#!/bin/bash

username="root"
ips="x.x.x.1 x.x.x.2 x.x.x.3 x.x.x.4 x.x.x.5"
passwords=("password" "passwords" "passwords" "passwords" "passwords")
names=("rmi1" "rmi2" "rmi3" "rmi4" "rmi5")
count=0
for ip in $ips
do
    expect ./ssh_key.exp $username $ip ${passwords[$count]}
    name=ssh_${names[$count]}
    echo '#!/bin/bash' > $name
    echo ssh $username@$ip >> $name
    chmod 771 $name
    count=`expr $count + 1`
done
ln -s `pwd`/ssh_* /usr/bin
