#!/bin/bash
paths="ImageServer LabelServer AtlasServer"
ps -ef | grep Server | grep -v grep | grep -v Maven | awk {'print $2'} | xargs kill -9
for path in $paths
do 
    cd /home/gongweixin/program/$path 
    mvn clean
    mvn package -DskipTests appassembler:assemble
    cmd=${path:0:5}
    cmd=`echo $cmd | tr "A-Z" "a-z"`
    nohup sh ./target/appassembler/bin/$cmd &
done
sleep 3
ps -ef | grep Server | grep -v grep | grep -v Maven | wc -l